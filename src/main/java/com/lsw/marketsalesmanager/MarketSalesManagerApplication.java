package com.lsw.marketsalesmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarketSalesManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarketSalesManagerApplication.class, args);
    }

}
