package com.lsw.marketsalesmanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DealMethod {

    CASH("현금"),
    CREDIT_CARD("카드"),
    WIRE("계좌이체");

    private final String dealMethodName;
}
