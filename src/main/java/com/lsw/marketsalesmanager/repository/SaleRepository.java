package com.lsw.marketsalesmanager.repository;

import com.lsw.marketsalesmanager.entity.Sale;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SaleRepository extends JpaRepository<Sale, Long> {
}
